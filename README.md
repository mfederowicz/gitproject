# README #

Jest to repozytorium szkoleniowe

### Tematyka szkolenia ###


* 5 minut o historii GIT
* Tworzenie repozytorium
* Dodawanie plików i kasowanie
* Commitowanie
* ....

### Dodatkowe materiały ###

* https://www.atlassian.com/git/
* Migracja repo SVN -> GIT : https://www.atlassian.com/git/migration
* Workflow : https://www.atlassian.com/git/workflows
